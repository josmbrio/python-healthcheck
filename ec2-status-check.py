import boto3
import schedule

ec2_client = boto3.client('ec2', region_name="us-east-1")
ec2_resource = boto3.resource('ec2', region_name="us-east-1")

# This shows more information, not only state and status
#reservations = ec2_client.describe_instances()
#for reservation in reservations["Reservations"]:
#    for instance in reservation["Instances"]:
#        print(f'Instance {instance["InstanceId"]} is {instance["State"]["Name"]}')


# This shows state and status of instance
def check_instance_status():
    statuses = ec2_client.describe_instance_status(
        IncludeAllInstances=True
    )
    for status in statuses["InstanceStatuses"]:
        int_status = status["InstanceStatus"]["Status"]
        sys_status = status["SystemStatus"]["Status"]
        state = status["InstanceState"]["Name"]
        instanceid = status["InstanceId"]
        print(f'Instance {instanceid} is {state} with instance status {int_status} and system status {sys_status}')
        print("############################")

schedule.every(5).seconds.do(check_instance_status)
#schedule.every(1).monday.at("1:00").do(check_instance_status)

while True:
    schedule.run_pending()