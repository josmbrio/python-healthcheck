import boto3

ec2_client_virginia = boto3.client('ec2', region_name="us-east-1")
ec2_resource_virginia = boto3.resource('ec2', region_name="us-east-1")

ec2_client_frankfurt = boto3.client('ec2', region_name="eu-central-1")
ec2_resource_frankfurt = boto3.resource('ec2', region_name="eu-central-1")

instances_ids_virgina = []
instances_ids_frankfurt = []

instances_virginia = ec2_client_virginia.describe_instances()["Reservations"]
for res in instances_virginia:
    instances = res["Instances"]
    for ins in instances:
        instances_ids_virgina.append(ins["InstanceId"])

response = ec2_resource_virginia.create_tags(
    Resources=instances_ids_virgina,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

instances_frankfurt = ec2_client_frankfurt.describe_instances()["Reservations"]
for res in instances_frankfurt:
    instances = res["Instances"]
    for ins in instances:
        instances_ids_frankfurt.append(ins["InstanceId"])

response = ec2_resource_frankfurt.create_tags(
    Resources=instances_ids_frankfurt,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)
